package com.company;

import java.util.Scanner;

public class Main {
    private static final Scanner scanner = new Scanner(System.in).useDelimiter("\n");

    public static void main(String[] args) {
        Account account = new Account();
        String selection;
        do {
            System.out.println("============Subas Bahadur Bank Information System===============");
            System.out.println("1. Set your account information");
            System.out.println("2. Deposit Amount");
            System.out.println("3. Withdraw Amount");
            System.out.println("4. Display Account Information");
            System.out.println("5. Exit");
            System.out.println("Enter number of your choice");
            int choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    account = setUpAccount();
                    break;
                case 2:
                    depositAmount(account);
                    break;
                case 3:
                    withdrawAmount(account);
                    break;
                case 4:
                    displayAccountInfo(account);
                    break;
                case 5:
                   System.exit(0);
                default:
                    System.out.println("Thank you!!!");
            }
            System.out.println("Do you want to continue?");
            System.out.println("Enter Y or N");
            selection = scanner.next();
        } while (selection.equalsIgnoreCase("Y"));
    }

    private static void displayAccountInfo(Account account) {
        System.out.println("-----------Account Details----------");
        System.out.println("Depositor Name:" + account.getDepositorName());
        System.out.println("Account Number:" + account.getAccountNUmber());
        System.out.println("Balance Amount:" + account.getBalanceAmount());
    }

    private static void withdrawAmount(Account account) {
        if(account.getBalanceAmount() != null) {
            System.out.println("Enter amount to withdraw");
            Double withdrawAmount = scanner.nextDouble();
            if (withdrawAmount > account.getBalanceAmount()) {
                System.out.println("Insufficient Balance");
            } else {
                account.setBalanceAmount(account.getBalanceAmount() - withdrawAmount);
                System.out.println("Withdraw amount :" + withdrawAmount);
                System.out.println("Remaining amount :" + account.getBalanceAmount());
            }
        }
    }

    private static void depositAmount(Account account) {
        if(account.getBalanceAmount() != null) {
            System.out.println("Enter amount to deposit");
            Double amount = scanner.nextDouble();
            account.setBalanceAmount(account.getBalanceAmount() + amount);
            System.out.println("Remaining amount :" + account.getBalanceAmount());
        }
    }

    private static Account setUpAccount() {
        Account account = new Account();
        System.out.println("Enter the depositor name:");
        String depositorName = scanner.next();
        System.out.println("Enter Account number:");
        String accountNumber = scanner.next();
        System.out.println("Enter the account type");
        String accountType = scanner.next();
        System.out.println("Enter the amount to deposit");
        Double depositAmount = scanner.nextDouble();

        account.setDepositorName(depositorName);
        account.setAccountNUmber(accountNumber);
        account.setAccountType(accountType);
        account.setBalanceAmount(depositAmount);
        return account;
        //TODO: uncomment this for the 10th question
//        return new Account(depositorName,accountNumber,accountType,depositAmount);
    }
}

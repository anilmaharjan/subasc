package com.company;

public class Account {
    public String depositorName;
    private String accountNUmber;
    private String accountType;
    private Double balanceAmount;

    public Account(String depositorName, String accountNUmber, String accountType, Double balanceAmount) {
        this.depositorName = depositorName;
        this.accountNUmber = accountNUmber;
        this.accountType = accountType;
        this.balanceAmount = balanceAmount;
    }

    public Account() {
    }

    public String getDepositorName() {
        return depositorName;
    }

    public void setDepositorName(String depositorName) {
        this.depositorName = depositorName;
    }

    public String getAccountNUmber() {
        return accountNUmber;
    }

    public void setAccountNUmber(String accountNUmber) {
        this.accountNUmber = accountNUmber;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public Double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(Double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }
}
